import Connection.Connection;

import Connection.messages.classes.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;

import SQLHandler.*; // Own package for sql management
import ServerFunctions.ServerFunctions;
import com.mysql.cj.x.protobuf.MysqlxCrud;


public class SocketServer {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        /*Objektumtesztek, a későbbi lekérdezett profilokra és termékekre,
          az egyes elemeket objektumként tároljuk és ezeket küldjük a kliens felé*/

        // adott email címmel rendelkező felhasználó lekérdezése
        ArrayList<Profile> ps = SelectCommands.selectUser("abel@proba.com");
        Profile p = ps.get(0);
        if(p != null)
        {
            System.out.println(p.toString());
        }

        // összes termék listázása mindegy az id
        ArrayList<MainCategory> mcArray = SelectCommands.selectMainCategory(-1, true);
        for(MainCategory i: mcArray)
        {
            System.out.println(i.toString());
        }

        // csak a 3-as id-jú kategória kiaíratása
        ArrayList<MainCategory> mcArray2 = SelectCommands.selectMainCategory(3, false);
        for(MainCategory i: mcArray2)
        {
            System.out.println(i.toString());
        }

        // csak az 5ös id-jú alkategória kiíratása
        ArrayList<SubCategory> scArray2 = SelectCommands.selectSubCategory(5, false);
        for(SubCategory i: scArray2)
        {
            System.out.println(i.toString());
        }

        // 10 id-jú termék lekérdezése
        ArrayList<Product> scArray3 = SelectCommands.selectProduct(10, false);
        for(Product i: scArray3)
        {
            System.out.println(i.toString());
        }

        // user autentikáció tesztelése

        ArrayList<Profile> p2 = SelectCommands.selectUser("abel@proba.com");
        p = p2.get(0);
        if(p != null)
        {
            System.out.println(p2.toString());
        }

        String pw = "dcd29b14b60d84a8fa52d76ef24c561df9a51103";
        boolean valid = ServerFunctions.authenticateUser("janos@proba.com", pw);
        if(valid)
        {
            System.out.println("User exists and the password is correct");
        }
        else
        {
            System.out.println("Wrong password");
        }

        // insert-ek tesztelése
        insertTest();

        // sql tesztelesek vege

        //KLIENS OLDALRA
        //ClientSerialization();

        Connection connection = Connection.getInstance();
        connection.setServerPort(3333);
        connection.run();
    }

    private static void ClientSerialization() {
        //ez kezeli majd a kapcsolatot, ha letrejon
        Socket client = null;
        //szerializalas operaciok kezelese
        ObjectOutputStream out = null;
        ObjectInputStream in = null;

        try{
            //melyik socketen kommunikaljon a kliens
            client = new Socket("127.0.0.1", 8888);

            out = new ObjectOutputStream(client.getOutputStream());
            in = new ObjectInputStream(client.getInputStream());

            //letrehozzuk az objektumot es atadjuk a szervernek
            Profile user1 = new Profile(1, "yes", "yes3", "gmail.com", "asdpass", "067055522233", "bp");
            out.writeObject(user1);
            out.flush();

            out.close();
            in.close();
            client.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void insertTest()
    {
        // az id-k azért -1 -ek, mert automatikusan kerülnek generálásra, és az sql utasításba amúgy sem kerülnek bele
        Profile user = new Profile(-1, "Géza", "Kiss", "geza@kiss.com", "password", "1234567", "Budapest");
        SubCategory sc = new SubCategory(-1, 3, "TV");
        MainCategory mc = new MainCategory(-1, "Elektronika");
        //Product product = new Product(-1, 110, 10, 12500, "", "Lcd Tv", "Ez egy Tv", "broken");
        Review rv = new Review(-1, 1,1, 5);
        Purchase purchase = new Purchase(-1, 11, 11, "");
        Wishlist wl = new Wishlist(-1, 11);
        CategoryWishlist cw = new CategoryWishlist(5, 1);
        ProductWishlist pw = new ProductWishlist(11, 1);
        Bidding bidding = new Bidding(-1, 11, -1, "", "");
        Bid bid = new Bid(-1, 11, 1, 500);

        boolean success = false;

//        success = ic.insertUser(user);
        //success = ic.insertSubCategory(sc);
        //success = ic.insertMainCategory(mc);
        //success = ic.insertProduct(product);
        //success = ic.insertPurchase(purchase);
        //success = ic.insertReview(rv); // csak akkor működik, ha létező vasarlas_id -t adunk meg(idegenkulcs megkötés)
        //success = ic.insertWishList(wl);
        //success = ic.insertCategoryWishList(cw);
        //success = ic.insertProductInWishList(pw);

        // ennél a kezdő és vég időpont beszúráskor ugyanaz, a végidőpont úgyis updattel kerül majd be, de erre lehet mást is kitalálni, csak üres értéket nem fogad el
        //success = ic.insertBidding(bidding);

        success = InsertCommands.insertBid(bid);

        if(success)
        {
            System.out.println("Success");
        }
    }
}
