package ServerFunctions;

import Connection.utility.BidHandler;
import Connection.utility.UploadHandler;
import Connection.messages.classes.Bid;
import Connection.messages.classes.Bidding;
import Connection.messages.classes.Product;
import Connection.messages.classes.Profile;
import Connection.messages.types.NotifyBidMessage;
import Connection.messages.types.NotifyProductMessage;
import SQLHandler.SelectCommands;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ServerFunctions implements UploadHandler, BidHandler {

    private final HashMap<String, ObjectOutputStream> clients = new HashMap<>();
    private static ServerFunctions instance = new ServerFunctions();

    private ServerFunctions() {
    }

    public static ServerFunctions getInstance() {
        return instance;
    }

    // további függvények, amik kellenek majd:
    // - ertekelesek kiszámolása
    // - licitálás menedzselése
    // - felhasználók értesítése: licit, kívánságlista

    // a jelszó már titkosítva érkezik, ezért elég összehasonlítani az adatbázisban lévővel
    public static boolean authenticateUser(String email, String password) {
        boolean valid = false;

        ArrayList<Profile> users = SelectCommands.selectUser(email);
        Profile user = users.get(0);
        if (user != null) {
            if (user.getPassword().equals(password)) {
                valid = true;
            } else {
                System.out.println("Password is incorrect");
            }
        } else {
            System.out.println("User not exists");
        }

        return valid;
    }

    /**
     * Product beszúrásnál ez a függvény fut le
     *
     * @param content - az InsertMessage content-je
     */
    @Override
    public void onUpload(Serializable content) {
        new Thread(() -> {
            Product product = (Product) content;
            String category =
                    SelectCommands
                            .selectSubCategory(product.getSub_category_id(), false)
                            .get(0)
                            .getSub_category_name();
            ArrayList<String> emails = SelectCommands.selectUserEmailsInterestedIn(category);
            for (Map.Entry<String, ObjectOutputStream> kv : clients.entrySet()) {
                //ha az email-ek között ott van a kliens email-je, akkor kapja az értesítést
                if (emails.contains(kv.getKey()))
                    try {
                        NotifyProductMessage notifyProductMessage = new NotifyProductMessage();
                        notifyProductMessage.setContent(content);
                        ObjectOutputStream outputStream = kv.getValue();
                        outputStream.writeObject(notifyProductMessage);
                        outputStream.flush();
                        outputStream.reset();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }).start();

    }

    public void registerClient(String id, ObjectOutputStream output) {
        clients.put(id, output);
    }

    /**
     * Mindenkinek küld egy értesítés új licit esetén
     * @param content
     */
    @Override
    public void onBidIncrease(Serializable content) {
        new Thread(() -> {
            //licitálás ID-ja, amire valaki tett egy licitet
            int biddingID = ((Bid)content).getBidding_id();
            //maga a licitálás, hogy megtudjuk melyik termékre licitáltak
            Bidding bidding =
                    SelectCommands.selectBidding(biddingID, false)
                    .get(0);
            //licitált termék ID-ja
            int productIDUnderBidding = bidding.getProduct_id();
            //maga a termék, amire licitáltak
            Product product =
                    SelectCommands.selectProduct(productIDUnderBidding, false).get(0);
            //felhasználók email-jei, akik licitáltak erre a termékre
            ArrayList<String> emails =
                    SelectCommands.selectUserEmailsInterestedIn(biddingID);
            for (Map.Entry<String, ObjectOutputStream> kv : clients.entrySet()) {
                //ha az email-ek között ott van a kliens email-je, akkor kapja az értesítést
                if (emails.contains(kv.getKey()))
                    try {
                        NotifyBidMessage notifyBidMessage = new NotifyBidMessage();
                        notifyBidMessage.setContent(product);
                        ObjectOutputStream outputStream = kv.getValue();
                        outputStream.writeObject(notifyBidMessage);
                        outputStream.flush();
                        outputStream.reset();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }).start();
    }
}
