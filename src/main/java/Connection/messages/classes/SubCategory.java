package Connection.messages.classes;

import java.io.Serializable;

public class SubCategory implements Serializable, IItem {
    int sub_category_id, main_category_id;
    String sub_category_name;

    public SubCategory(int sub_category_id, int main_category_id, String sub_category_name) {
        this.sub_category_id = sub_category_id;
        this.main_category_id = main_category_id;
        this.sub_category_name = sub_category_name;
    }

    public SubCategory() {
    }

    @Override
    public String toString() {
        return "sub category id: " + sub_category_id + "\n" +
                "main category id: " + main_category_id + "\n" +
                "sub category name: " + sub_category_name;
    }

    public int getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(int sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public int getMain_category_id() {
        return main_category_id;
    }

    public void setMain_category_id(int main_category_id) {
        this.main_category_id = main_category_id;
    }

    public String getSub_category_name() {
        return sub_category_name;
    }

    public void setSub_category_name(String sub_category_name) {
        this.sub_category_name = sub_category_name;
    }

    @Override
    public int getID() {
        return sub_category_id;
    }
}
