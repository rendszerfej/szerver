package Connection.messages.classes;

import java.io.Serializable;

public class Purchase implements Serializable, IItem {
    int purchase_id, profile_id, product_id;
    String purchase_date;

    public Purchase() {
    }

    public Purchase(int purchase_id, int profile_id, int product_id, String purchase_date) {
        this.purchase_id = purchase_id;
        this.profile_id = profile_id;
        this.product_id = product_id;
        this.purchase_date = purchase_date;
    }

    @Override
    public String toString() {
        return "purchase id: " + purchase_id + "\n" +
                "profile id: " + profile_id + "\n" +
                "product id: " + product_id + "\n" +
                "purchase date: " + purchase_date;
    }

    public int getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(int purchase_id) {
        this.purchase_id = purchase_id;
    }

    public int getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(int profile_id) {
        this.profile_id = profile_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(String purchase_date) {
        this.purchase_date = purchase_date;
    }

    @Override
    public int getID() {
        return purchase_id;
    }
}
