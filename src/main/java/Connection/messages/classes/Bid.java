package Connection.messages.classes;

import java.io.Serializable;

public class Bid implements Serializable, IItem {
    int bid_id, bidder_id, bidding_id, bid_value;

    public Bid(){}

    public Bid(int bid_id, int bidder_id, int bidding_id, int bid_value) {
        this.bid_id = bid_id;
        this.bidder_id = bidder_id;
        this.bidding_id = bidding_id;
        this.bid_value = bid_value;
    }

    @Override
    public String toString() {
        return "bid id: " + bid_id + "\n" +
                "bidder id: " + bidder_id + "\n" +
                "bidding id: " + bidding_id + "\n" +
                "bid value: " + bid_value;
    }

    public int getBid_id() {
        return bid_id;
    }

    public void setBid_id(int bid_id) {
        this.bid_id = bid_id;
    }

    public int getBidder_id() {
        return bidder_id;
    }

    public void setBidder_id(int bidder_id) {
        this.bidder_id = bidder_id;
    }

    public int getBidding_id() {
        return bidding_id;
    }

    public void setBidding_id(int bidding_id) {
        this.bidding_id = bidding_id;
    }

    public int getBid_value() {
        return bid_value;
    }

    public void setBid_value(int bid_value) {
        this.bid_value = bid_value;
    }

    @Override
    public int getID() {
        return bid_id;
    }
}
