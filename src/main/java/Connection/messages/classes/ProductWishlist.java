package Connection.messages.classes;

import java.io.Serializable;

public class ProductWishlist implements Serializable, IItem {
    int product_id, wishlist_id;

    public ProductWishlist() {
    }

    public ProductWishlist(int product_id, int wishlist_id) {
        this.product_id = product_id;
        this.wishlist_id = wishlist_id;
    }

    @Override
    public String toString() {
        return "product id: " + product_id + "\n" +
                "wishlist id: " + wishlist_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getWishlist_id() {
        return wishlist_id;
    }

    public void setWishlist_id(int wishlist_id) {
        this.wishlist_id = wishlist_id;
    }

    @Override
    public int getID() {
        return product_id;
    }
}
