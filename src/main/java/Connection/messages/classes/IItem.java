package Connection.messages.classes;

import java.io.Serializable;

public interface IItem extends Serializable {
    int getID();
}
