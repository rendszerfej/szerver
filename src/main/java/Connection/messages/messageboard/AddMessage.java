package Connection.messages.messageboard;

import Connection.messages.Message;

/**
 * Ezzel a message-el adhatunk a server-hez fórum üzenetet.
 */
public class AddMessage extends Message {

    private BoardMessage message;

    public AddMessage(){

    }

    public BoardMessage getMessage() {
        return message;
    }

    public void setMessage(BoardMessage message) {
        this.message = message;
    }
}
