package Connection.messages.messageboard;

import Connection.messages.Message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Lekéri a serverről az összes fórum üzenetet egy listába.
 */
public class ListMessage extends Message {

    private List<Serializable> serializables;

    public ListMessage(){
        this.serializables = new ArrayList<>();
    }

    public List<Serializable> getSerializables() {
        return serializables;
    }

    public void setSerializables(List<Serializable> serializables) {
        this.serializables = serializables;
    }
}
