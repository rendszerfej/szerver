package Connection.messages;

import java.io.Serializable;

/**
 * A "messageboard-ban található message-k mind ebból az osztályból származnak.
 * Valamelyik nem is tartalmaz plusz adattagot, mint például a CloseMessage.
 * A CloseMessage-ról tudni fogja a server, hogy bontani akarja a kliens a kapcsolatot,
 * így felesleges plusz adatot küldeni, a CloseMessage szülő osztály adatain kívül.
 */
public abstract class Message implements Serializable {

    /**
     * Ez az adattag fontos, mert ennek egyeznie kell, a server-en és a kliensen is
     * Folyamatosan növelni kell, hogyha változtatunk a class-on
     */
    private static final long serialVersionUID = 7L;

    private String clientId;

    private Serializable content;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Serializable getContent() {
        return content;
    }

    public void setContent(Serializable content) {
        this.content = content;
    }
}
