package Connection;

import Connection.messages.Message;
import Connection.messages.classes.*;
import Connection.messages.messageboard.AckMessage;
import Connection.messages.messageboard.CloseMessage;
import Connection.messages.messageboard.ErrorMessage;
import Connection.messages.messageboard.ListMessage;
import Connection.messages.types.*;
import Connection.utility.BidHandler;
import Connection.utility.UploadHandler;
import SQLHandler.DeleteCommands;
import SQLHandler.InsertCommands;
import SQLHandler.SelectCommands;
import SQLHandler.UpdateCommands;
import ServerFunctions.ServerFunctions;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Ez a példa nem használ adatbázist, így a MessageBoardProtocol osztály tárolja
 * a fórum üzeneteket a messages listában
 */
public class MessageBoardProtocol extends Protocol {

    private final UploadHandler uploadHandler = ServerFunctions.getInstance();

    private final BidHandler bidHandler = ServerFunctions.getInstance();

    public MessageBoardProtocol() {
        initMessageHandlers();
    }

    /**
     * Ide kerülnek majd az adatbázisba való feltöltések, módosítások
     */
    private void initMessageHandlers() {
        AckMessage ackMessage = new AckMessage();
        //SELECT utasítások
        registerHandler(SelectMessage.class, (msg) -> {
            if (msg.getContent().getClass() == Bid.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectBid(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == Bidding.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectBidding(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == CategoryWishlist.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectCategoryWishlist(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == MainCategory.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectMainCategory(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == Product.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectProduct(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == ProductWishlist.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectProductWishlist(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == Profile.class) {
                Profile p = (Profile) msg.getContent();
                if(p.getEmail()!=null){
                ListMessage lm = new ListMessage();

                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectUser(p.getEmail()));
                lm.setSerializables(list);
                return lm;
                }
                else{
                    ListMessage lm = new ListMessage();

                    ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectUsers());
                    lm.setSerializables(list);
                    return lm;
                }
            } else if (msg.getContent().getClass() == Purchase.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectPurchase(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == Review.class) {
                ListMessage lm = new ListMessage();
                //vagy ezt használjuk vagy azt, hogy mindenkiét lekérdezi
                //float avg = SelectCommands.selectRatingOf(/*user email*/);
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectReview(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == SubCategory.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectSubCategory(0, true));
                lm.setSerializables(list);
                return lm;
            } else if (msg.getContent().getClass() == Wishlist.class) {
                ListMessage lm = new ListMessage();
                ArrayList<Serializable> list = new ArrayList<>(SelectCommands.selectWishlist(0, true));
                lm.setSerializables(list);
                return lm;
            } else
                return new ErrorMessage("You can't SELECT this class type!");
        });
        //UPDATE utasítások
        registerHandler(UpdateMessage.class, (msg) -> {
            if (msg.getContent().getClass() == Profile.class) {
                //UPDATE class type
                return ackMessage;
            }
            else if (msg.getContent().getClass() == Product.class) {
                //UPDATE class type
                uploadHandler.onUpload(msg.getContent());
                return ackMessage;
            }
            else if (msg.getContent().getClass() == ProductWishlist.class) {
                //UPDATE class type
                return ackMessage;
            }
            else if (msg.getContent().getClass() == CategoryWishlist.class) {
                //UPDATE class type
                return ackMessage;

            }
            else if(msg.getContent().getClass() == Bidding.class){
                Bidding bidding = (Bidding) msg.getContent();
                System.out.println(bidding.getFinal_price()+" "+bidding.getProduct_id());
                UpdateCommands.updateBidding(bidding.getFinal_price(),bidding.getProduct_id());

                return ackMessage;


            }
            else
                return new ErrorMessage("You can't UPDATE this class type!");
        });
        //INSERT utasítások
        registerHandler(InsertMessage.class, (msg) -> {
            if (msg.getContent().getClass() == Bid.class) {
                bidHandler.onBidIncrease(msg.getContent());
                boolean result = InsertCommands.insertBid((Bid) msg.getContent());
                if (result)
                    return ackMessage;
                return new ErrorMessage("Insert failed!");

            } else if (msg.getContent().getClass() == Bidding.class) {
                boolean result = InsertCommands.insertBidding((Bidding) msg.getContent());
                if (result)
                    return ackMessage;
                return new ErrorMessage("Insert failed!");

            } else if (msg.getContent().getClass() == CategoryWishlist.class) {
                boolean result = InsertCommands.insertCategoryWishList((CategoryWishlist) msg.getContent());
                if (result)
                    return ackMessage;
                return new ErrorMessage("Insert failed!");

            } else if (msg.getContent().getClass() == Product.class) {
                boolean result = InsertCommands.insertProduct((Product) msg.getContent());
                if (result) {
                    uploadHandler.onUpload(msg.getContent());
                    return ackMessage;
                }
                return new ErrorMessage("Insert failed!");

            } else if (msg.getContent().getClass() == ProductWishlist.class) {
                boolean result = InsertCommands.insertProductInWishList((ProductWishlist) msg.getContent());
                if (result)
                    return ackMessage;
                return new ErrorMessage("Insert failed!");

            } else if (msg.getContent().getClass() == Profile.class) {
                boolean result = InsertCommands.insertUser((Profile) msg.getContent());
                if (result)
                    return ackMessage;
                return new ErrorMessage("Insert failed!");

            } else if (msg.getContent().getClass() == Purchase.class) {
                boolean result = InsertCommands.insertPurchase((Purchase) msg.getContent());
                if (result)
                    return ackMessage;
                return new ErrorMessage("Insert failed!");

            } else if (msg.getContent().getClass() == Review.class) {
                boolean result = InsertCommands.insertReview((Review) msg.getContent());
                if (result)
                    return ackMessage;
                return new ErrorMessage("Insert failed!");

            } else if (msg.getContent().getClass() == Wishlist.class) {
                boolean result = InsertCommands.insertWishList((Wishlist) msg.getContent());
                if (result)
                    return ackMessage;
                return new ErrorMessage("Insert failed!");

            } else
                return new ErrorMessage("You can't INSERT this class type!");
        });
        //DELETE utasítások
        registerHandler(DeleteMessage.class, (msg) -> {
            if (msg.getContent().getClass() == Product.class) {
                //DELETE class type
                return ackMessage;

            }
            else if(msg.getContent().getClass() == CategoryWishlist.class){
                CategoryWishlist w = (CategoryWishlist)msg.getContent();
                boolean res = DeleteCommands.deleteWishListCategory(w.getWishlist_id(),w.getSub_category_id());

                return ackMessage;
                }


            else
                return new ErrorMessage("You cant");
        });
        //LOGIN
        registerHandler(LoginMessage.class, (msg) -> {
            if (msg.getContent().getClass() == Profile.class) {
                Profile p = (Profile) msg.getContent();
                AckMessage ack = new AckMessage();
                if(ServerFunctions.authenticateUser(p.getEmail(),p.getPassword()))
                    return ackMessage;
                else
                    return new ErrorMessage("Login failed");
            } else
                return new ErrorMessage("Login failed");
        });
    }



    /**
     * Ha a paraméterben kapott bejövő message egy CloseMessage, akkor igaz értéke lesz.
     *
     * @param input
     * @return
     */
    @Override
    public boolean isClosingSignal(Message input) {
        return (input instanceof CloseMessage);
    }

}
