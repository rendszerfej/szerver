package Connection;

import Connection.messages.Message;
import Connection.messages.messageboard.ErrorMessage;
import Connection.utility.MessageHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * A Protocol osztály annyit tesz, hogy egy map-ben felsorolhatjuk,
 * hogy melyik Message alosztályunkhoz, melyik MessageHandler tartozik.
 */
public abstract class Protocol {

    private final Map<Class, MessageHandler> handlers;

    public Protocol(){
        this.handlers = new HashMap<>();
    }

    /**
     * Eltároljuk egy map-ben, hogy az adott osztályhoz melyik MessageHandler tartozik
     * @param type osztály típusa
     * @param handler MessageHandler
     */
    protected void registerHandler(Class type, MessageHandler handler){
        handlers.put(type, handler);
    }

    /**
     * Feldolgozza a Message-t, ha ahhoz talál MessageHandler-t a map-ben,
     * akkor azt a Message típust adja vissza, ilyen például:
     *      -AddMessage (amikor a kliens ADD-ot ír be)
     *      -ListMessage (amikor a kliens LIST-et ír be)
     * Amennyiben nem található a Message típushoz MessageHandler,
     * akkor egy ErrorMessage-el tér vissza.
     * @param input
     * @return
     */
    public Message process(Message input){
        if(handlers.containsKey(input.getClass())){
            try {
                return handlers.get(input.getClass()).handleMessage(input);
            } catch (Exception e) {
                return new ErrorMessage(e.getMessage());
            }
        }
        else{
            return new ErrorMessage("Unsupported operation!");
        }
    }

    public abstract boolean isClosingSignal(Message input);

}
