package Connection.utility;

import Connection.Connection;
import Connection.messages.Message;

/**
 * Azok az interface-ek, amiknek csak egy method-juk van, azokat el kell látni
 * ezzel az annotation-nel @FunctionalInterface.
 * A MessageHandler interface-t, azok az osztályok implementálják, akik kezelni
 * tudják a beérkező üzeneteket. Ebben a példában lambda függvényként implementálják
 * ezt az interface-t.
 */
@FunctionalInterface
public interface MessageHandler {

    public Message handleMessage(Message input) throws Exception;
}
