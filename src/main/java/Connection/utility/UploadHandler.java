package Connection.utility;

import Connection.Connection;

import java.io.Serializable;

@FunctionalInterface
public interface UploadHandler {
    public void onUpload(Serializable content);
}
