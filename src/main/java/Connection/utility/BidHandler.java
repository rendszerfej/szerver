package Connection.utility;

import Connection.Connection;

import java.io.Serializable;

@FunctionalInterface
public interface BidHandler {
    void onBidIncrease(Serializable bid);
}
