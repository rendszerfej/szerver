package SQLHandler;

import Connection.messages.classes.*;

import java.sql.*;
import java.util.ArrayList;

public class SelectCommands extends SQLCommands {
    //+ = kesz, - = nincs
    // + alkategoria
    // + ertekeles
    // + felhasznalo
    // + fokategoria
    // + kategoria_kivansaglistaban
    // + kivansaglista
    // + licit
    // + licitalas
    // + termek
    // + termek kivansaglistaban
    // + vasarlas

    public static ArrayList<Profile> selectUser(String email)
    {
        ArrayList<Profile> p = new ArrayList<>();
        String sql = "Select * from felhasznalo where email_cim = '" + email + "'";

        try
        {
            //Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/rendszerfejlesztes", "root", "");
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int profile_id = rs.getInt("felhasznalo_id");
                String first_name = rs.getString("keresztnev");
                String last_name = rs.getString("vezeteknev");
                String email_address = rs.getString("email_cim");
                String password = rs.getString("password");
                String phone_number = rs.getString("telefonszam");
                String city = rs.getString("telepules_nev");

                p.add(new Profile(profile_id, first_name, last_name, email_address, password, phone_number, city));
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return p;
    }
    public static ArrayList<Profile> selectUsers()
    {
        ArrayList<Profile> p = new ArrayList<>();
        String sql = "Select * from felhasznalo ";

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int profile_id = rs.getInt("felhasznalo_id");
                String first_name = rs.getString("keresztnev");
                String last_name = rs.getString("vezeteknev");
                String email_address = rs.getString("email_cim");
                String password = "";
                String phone_number = rs.getString("telefonszam");
                String city = rs.getString("telepules_nev");

                p.add(new Profile(profile_id, first_name, last_name, email_address, password, phone_number, city));
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return p;
    }

    public static ArrayList<MainCategory> selectMainCategory(int id, boolean all)
    {
        ArrayList<MainCategory> mcArray = new ArrayList<MainCategory>();
        String sql = "Select * from fokategoria where fokategoria_id = " + id + "";
        if(all)
        {
            sql = "Select * from fokategoria";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int main_category_id = rs.getInt("fokategoria_id");
                String main_category_name = rs.getString("fokategoria_megnevezes");

                MainCategory mc = new MainCategory(main_category_id,  main_category_name);
                mcArray.add(mc);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return mcArray;
    }

    public static ArrayList<CategoryWishlist> selectCategoryWishlist(int id, boolean all)
    {
        ArrayList<CategoryWishlist> cwArray = new ArrayList<CategoryWishlist>();
        String sql = "Select * from kategoria_kivansaglistaban where kategoria_ID = " + id + "";
        if(all)
        {
            sql = "Select * from kategoria_kivansaglistaban";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int category_id = rs.getInt("kategoria_ID");
                int wishlist_id = rs.getInt("kivansaglista_ID");

                CategoryWishlist cw = new CategoryWishlist(category_id, wishlist_id);
                cwArray.add(cw);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return cwArray;
    }

    public static ArrayList<Wishlist> selectWishlist(int id, boolean all)
    {
        ArrayList<Wishlist> wArray = new ArrayList<Wishlist>();
        String sql = "Select * from kivansaglista where kivansaglista_ID = " + id + "";
        if(all)
        {
            sql = "Select * from kivansaglista";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int wishlist_id = rs.getInt("kivansaglista_ID");
                int profile_id = rs.getInt("felhasznalo_ID");

                Wishlist w = new Wishlist(wishlist_id, profile_id);
                wArray.add(w);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return wArray;
    }

    public static ArrayList<Bid> selectBid(int id, boolean all)
    {
        ArrayList<Bid> bArray = new ArrayList<Bid>();
        String sql = "Select * from licit where licit_ID = " + id + "";
        if(all)
        {
            sql = "Select * from licit";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int bid_id = rs.getInt("licit_ID");
                int bidder_id = rs.getInt("licitalo_ID");
                int bidding_id = rs.getInt("licitalas_ID");
                int bid_value = rs.getInt("ertek");

                Bid b = new Bid(bid_id, bidder_id,bidding_id,bid_value);
                bArray.add(b);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return bArray;
    }

    public static ArrayList<Bidding> selectBidding(int id, boolean all)
    {
        ArrayList<Bidding> bngArray = new ArrayList<Bidding>();
        String sql = "Select * from licitalas where licitalas_ID = " + id + "";
        if(all)
        {
            sql = "Select * from licitalas";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int bidding_id = rs.getInt("licitalas_ID");
                int product_id = rs.getInt("termek_ID");
                Date date1 = rs.getDate("kezdo_idopont");
                String start_bidding_time = date1.toString();
                Date date2 = rs.getDate("veg_idopont");
                String end_bidding_time = date2.toString();
                int final_price = rs.getInt("vegso_ar");

                Bidding bng = new Bidding(bidding_id, product_id,final_price,start_bidding_time,end_bidding_time);
                bngArray.add(bng);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return bngArray;
    }

    public static ArrayList<SubCategory> selectSubCategory(int id, boolean all)
    {
        ArrayList<SubCategory> scArray = new ArrayList<SubCategory>();
        String sql = "Select * from alkategoria where alkategoria_id = " + id + "";
        if(all)
        {
            sql = "Select * from alkategoria";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int sub_category_id = rs.getInt("alkategoria_id");
                int main_category_id = rs.getInt("fokategoria_id");
                String sub_category_name = rs.getString("alkategoria_megnevezes");

                SubCategory sc = new SubCategory(sub_category_id, main_category_id,  sub_category_name);
                scArray.add(sc);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return scArray;
    }

    public static ArrayList<Review> selectReview(int id, boolean all)
    {
        ArrayList<Review> reviewArray = new ArrayList<Review>();
        String sql = "Select * from ertekeles where ertekeles_id = " + id + "";
        if(all)
        {
            sql = "Select * from ertekeles";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int review_id = rs.getInt("ertekeles_id");
                int product_id = rs.getInt("termek_ID");
                int profile_id = rs.getInt("felhasznalo_ID");
                int review_value = rs.getInt("ertek");

                Review r = new Review(review_id, product_id,profile_id , review_value);
                reviewArray.add(r);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return reviewArray;
    }

    public static ArrayList<Product> selectProduct(int id, boolean all)
    {
        ArrayList<Product> productArray = new ArrayList<Product>();
        String sql = "Select * from termek where termek_id = " + id + "";
        if(all)
        {
            sql = "Select * from termek order by meghirdetes_ideje desc";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                //public Product(int product_id, int profile_id, int price, String advertisement_date, String name, String description, String status)
                int product_id = rs.getInt("termek_id");
                int profile_id = rs.getInt("felhasznalo_id");
                int sub_category_id = rs.getInt("alkategoria_id");
                Date date = rs.getDate("meghirdetes_ideje");
                String advertisment_date = date.toString();
                String name = rs.getString("termek_megnevezes");
                String description = rs.getString("termek_leiras");
                int price = rs.getInt("termek_ar");
                String status = rs.getString("termek_allapot");

                Product p = new Product(product_id,profile_id,sub_category_id,price,advertisment_date,name,description,status);
                productArray.add(p);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return productArray;
    }

    public static ArrayList<ProductWishlist> selectProductWishlist(int id, boolean all)
    {
        ArrayList<ProductWishlist> pwArray = new ArrayList<ProductWishlist>();
        String sql = "Select * from termek_kivansaglistaban where termek_ID = " + id + "";
        if(all)
        {
            sql = "Select * from termek_kivansaglistaban";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int product_id = rs.getInt("termek_ID");
                int wishlist_id = rs.getInt("kivansaglista_ID");

                ProductWishlist pw = new ProductWishlist(product_id, wishlist_id);
                pwArray.add(pw);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return pwArray;
    }

    public static ArrayList<Purchase> selectPurchase(int id, boolean all)
    {
        ArrayList<Purchase> pArray = new ArrayList<Purchase>();
        String sql = "Select * from vasarlas where vasarlas_ID = " + id + "";
        if(all)
        {
            sql = "Select * from vasarlas";
        }

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                int purchase_id = rs.getInt("vasarlas_ID ");
                int profile_id = rs.getInt("felhasznalo_ID ");
                int product_id = rs.getInt("termek_ID ");
                Date date1 = rs.getDate("vasarlas_idopont");
                String purchase_date = date1.toString();

                Purchase p = new Purchase(purchase_id, profile_id, product_id, purchase_date);
                pArray.add(p);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return pArray;
    }

    public static ArrayList<String> selectUserEmailsInterestedIn(String category)
    {
        ArrayList<String> userEmails = new ArrayList<>();
        String sql = "SELECT fh.email_cim AS email " +
                "    FROM felhasznalo AS fh " +
                "    JOIN kivansaglista AS kl ON fh.felhasznalo_ID = kl.felhasznalo_ID " +
                "    JOIN kategoria_kivansaglistaban AS kkl ON kl.kivansaglista_ID = kkl.kivansaglista_ID " +
                "    JOIN alkategoria AS ak ON kkl.kategoria_ID = ak.alkategoria_ID " +
                "    WHERE ak.alkategoria_megnevezes = '" + category + "'";

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.printf("Result set is empty");
            }

            while(rs.next())
            {
                String sub_category_name = rs.getString("email");

                userEmails.add(sub_category_name);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return userEmails;
    }

    public static float selectRatingOf(String userEmail)
    {
        float average = 0;
        String sql =
                "SELECT AVG(e.szam_ertek) AS atlag " +
                "   FROM felhasznalo AS f " +
                "   JOIN termek AS t ON f.felhasznalo_ID = t.felhasznalo_id " +
                "   JOIN vasarlas AS v ON t.termek_id = t.termek_id " +
                "   JOIN ertekeles AS e ON v.vasarlas_ID = e.vasarlas_ID " +
                "   WHERE f.email_cim = '" + userEmail +"'";

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if(rs == null)
            {
                System.out.println("Result set is empty");
            }

            while(rs != null && rs.next())
            {
                average = rs.getFloat("atlag");
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return average;
    }

    public static ArrayList<String> selectUserEmailsInterestedIn(int biddingID)
    {
        ArrayList<String> userEmails = new ArrayList<>();
        String sql =
                "SELECT DISTINCT f.email_cim AS email " +
                "    FROM felhasznalo AS f " +
                "    JOIN licit l ON f.felhasznalo_ID = l.licitalo_ID " +
                "    JOIN licitalas l2 on l.licitalas_ID = l2.licitalas_ID " +
                "    WHERE l2.licitalas_ID ='" + biddingID + "'";

        try
        {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while(rs != null && rs.next())
            {
                String email = rs.getString("email");

                userEmails.add(email);
            }

            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return userEmails;
    }
}
