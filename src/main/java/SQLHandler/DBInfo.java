package SQLHandler;

/**
 * Így csak egy helyen kell változtatni ezeket az adatokat.
 * (persze, ha lenne az adatbázisnak különböző rangú felhasználói stb..., akkor más lenne)
 */
public class DBInfo {
    public static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/rendszerfejlesztes";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "";
}
