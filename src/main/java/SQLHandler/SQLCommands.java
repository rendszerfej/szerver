package SQLHandler;

public abstract class SQLCommands {
    protected static final String URL = DBInfo.DB_URL;
    protected static final String USER = DBInfo.DB_USER;
    protected static final String PASSWORD = DBInfo.DB_PASSWORD;
}
