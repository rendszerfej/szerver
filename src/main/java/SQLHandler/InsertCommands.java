package SQLHandler;

import Connection.messages.classes.*;

import java.sql.*;

public class InsertCommands extends SQLCommands {
    // + alkategoria
    // + ertekeles
    // + felhasznalo
    // + fokategoria
    // + kategoria_kivansaglistaban
    // + kivansaglista
    // + licit
    // + licitalas
    // + termek
    // + termek kivansaglistaban
    // + vasarlas

    public static boolean insertUser(Profile user) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO felhasznalo(keresztnev, vezeteknev, email_cim, password, telefonszam, telepules_nev) VALUES('" +
                user.getLast_name() + "','" + user.getFirst_name() + "','" + user.getEmail() + "','"
                + user.getPassword() + "','" + user.getPhone_number() + "','" + user.getCity() + "')";

        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertMainCategory(MainCategory category) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO fokategoria(fokategoria_megnevezes) VALUES('" +
                category.getMain_category_name() + "')";
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertSubCategory(SubCategory category) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO alkategoria(fokategoria_id, alkategoria_megnevezes) VALUES(" +
                category.getMain_category_id() + ",'" + category.getSub_category_name() + "')";
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertProduct(Product product) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO termek(felhasznalo_id, alkategoria_id,meghirdetes_ideje, termek_megnevezes, termek_leiras, termek_ar, termek_allapot) VALUES(" +
                product.getProfile_id() + "," + product.getSub_category_id() + ", current_time, '" + product.getName() + "','" +
                product.getDescription() + "'," + product.getPrice() + ",'" + product.getStatus() + "')";
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertReview(Review review) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO ertekeles(termek_ID, felhasznalo_ID,ertek) VALUES(" +
                review.getProduct_id() + ","+review.getProfile_id()+"," + review.getReview_value() + ")";

        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertPurchase(Purchase purchase) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO vasarlas(felhasznalo_ID, termek_ID, vasarlas_idopont) VALUES(" +
                purchase.getProfile_id() + "," + purchase.getProduct_id() + ", current_time" + ")";

        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertCategoryWishList(CategoryWishlist cw) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO kategoria_kivansaglistaban(kategoria_ID, kivansaglista_ID) VALUES(" +
                cw.getSub_category_id() + "," + cw.getWishlist_id() + ")";

        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertWishList(Wishlist wishlist) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO kivansaglista(felhasznalo_ID) VALUES(" +
                wishlist.getProfile_id() + ")";

        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertBid(Bid bid) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO licit(licitalo_ID, licitalas_ID, ertek) VALUES(" +
                bid.getBidder_id() + "," + bid.getBidding_id() + "," + bid.getBid_value() + ")";

        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertBidding(Bidding bidding) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO licitalas(termek_ID, kezdo_idopont, veg_idopont, vegso_ar) VALUES(" +
                bidding.getProduct_id() + ",current_time,'" + bidding.getEnd_bidding_time() + "'," + bidding.getFinal_price() + ")";

        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }

    public static boolean insertProductInWishList(ProductWishlist pw) {
        boolean success = false;

        Connection conn = null;
        Statement stmt = null;

        String sql = "INSERT INTO termek_kivansaglistaban(termek_ID, kivansaglista_id) VALUES(" +
                pw.getProduct_id() + "," + pw.getWishlist_id() + ")";

        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.close();
            success = true;
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return success;
    }
}
