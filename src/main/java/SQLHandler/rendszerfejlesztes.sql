-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Ápr 13. 13:50
-- Kiszolgáló verziója: 10.4.11-MariaDB
-- PHP verzió: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `rendszerfejlesztes`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `alkategoria`
--

CREATE TABLE `alkategoria` (
  `alkategoria_ID` int(11) NOT NULL,
  `fokategoria_ID` int(11) NOT NULL,
  `alkategoria_megnevezes` varchar(1000) COLLATE utf8mb4_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `alkategoria`
--

INSERT INTO `alkategoria` (`alkategoria_ID`, `fokategoria_ID`, `alkategoria_megnevezes`) VALUES
(1, 1, 'Mobiltelefon'),
(2, 1, 'TV'),
(3, 1, 'Fülhallgató, fejhallgató'),
(4, 2, 'Notebook'),
(5, 2, 'Monitor'),
(6, 2, 'Nyomtató'),
(7, 3, 'Porszívó'),
(8, 3, 'Hűtőszekrény'),
(9, 3, 'Mosógép'),
(10, 4, 'Cipő'),
(11, 4, 'Kabát'),
(13, 5, 'Kerékpár'),
(15, 8, 'Lego'),
(16, 8, 'Társasjáték'),
(17, 8, 'Puzzle'),
(20, 7, 'Autó akkumulátor'),
(21, 9, 'Fénymásoló'),
(22, 9, 'Forgószék'),
(23, 9, 'Számológép'),
(24, 4, 'Táska'),
(25, 5, 'Gördeszka'),
(26, 6, 'Fűnyíró'),
(27, 7, 'Motor bukósisak');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `ertekeles`
--

CREATE TABLE `ertekeles` (
  `ertekeles_ID` int(11) NOT NULL,
  `vasarlas_ID` int(11) NOT NULL,
  `szam_ertek` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `felhasznalo`
--

CREATE TABLE `felhasznalo` (
  `felhasznalo_ID` int(11) NOT NULL,
  `keresztnev` varchar(30) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `vezeteknev` varchar(30) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `email_cim` varchar(40) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `telefonszam` varchar(11) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `telepules_nev` varchar(40) COLLATE utf8mb4_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `felhasznalo`
--

INSERT INTO `felhasznalo` (`felhasznalo_ID`, `keresztnev`, `vezeteknev`, `email_cim`, `password`, `telefonszam`, `telepules_nev`) VALUES
(11, 'Nagy', 'László', 'laszlo@proba.com', 'cddd621824069f1ca21709c1fcc6d70974d7b9bf', '36314755347', 'Budapest'),
(12, 'Kovács', 'István', 'istvan@proba.com', 'be511dcb453180286c70938342cf819ef36ce390', '36314755348', 'Debrecen'),
(13, 'Tóth', 'József', 'jozsef@proba.com', '60a9d0e80b60dbb783954471874ae133d9f612d0', '36314755349', 'Szeged'),
(14, 'Szabó', 'János', 'janos@proba.com', 'dcd29b14b60d84a8fa52d76ef24c561df9a51103', '36314755350', 'Miskolc'),
(15, 'Horváth', 'Zoltán', 'zoltan@proba.com', 'b05508a73f3a7a9e4d6ff0c299a1effd5c145d41', '36314755351', 'Pécs'),
(16, 'Varga', 'Sándor', 'sandor@proba.com', '6b85c9fe945a33f674ad263c1d9a05fb1fe2f9ca', '36314755352', 'Győr'),
(17, 'Kiss', 'Gábor', 'gabor@proba.com', '2c30b7bc41fe3f87da8eebe250c23318082f1e7c', '36314755353', 'Nyíregyháza'),
(18, 'Molnár', 'Ferenc', 'ferenc@proba.com', '5bc9c2fe03d0f05c58700792282e21073ef0315c', '36314755354', 'Kecskemét'),
(19, 'Németh', 'Attila', 'attila@proba.com', '2f388f3e0b3988599219c89d86fca2a75cb4ed0b', '36314755355', 'Székesfehérvár'),
(20, 'Farkas', 'Péter', 'peter@proba.com', '328854132bf61a37c6b4a64be7b23d03b74f8f83', '36314755356', 'Szombathely'),
(21, 'Balogh', 'Tamás', 'tamas@proba.com', '6bc2599bba30046eddc036882d0fd79a62b67354', '36314755357', 'Szolnok'),
(22, 'Papp', 'Zsolt', 'zsolt@proba.com', 'b8cffb1e2b3f7f0e96afb8c6accafcb1a7041dbf', '36314755358', 'Tatabánya'),
(23, 'Lakatos', 'Tibor', 'tibor@proba.com', 'f98f7c718304c2f1aa1ec93cc741aa003cdff2d5', '36314755359', 'Kaposvár'),
(24, 'Takács', 'András', 'andras@proba.com', '2fe8f459c00229b584bdb39ccdefbaf86e28cbe6', '36314755360', 'Érd'),
(25, 'Juhász', 'Csaba', 'csaba@proba.com', 'd21012d78f7ab33ddb9c749cbd082fbf3cff9412', '36314755361', 'Veszprém'),
(26, 'Mészáros', 'Imre', 'imre@proba.com', '866d6d1b1fb911d918fffb3293f24ed0ca25f8e4', '36314755362', 'Békéscsaba'),
(27, 'Oláh', 'Lajos', 'lajos@proba.com', '5d7c67484124587eeabf4fc4dc65acfefd860086', '36314755363', 'Zalaegerszeg'),
(28, 'Simon', 'György', 'gyorgy@proba.com', 'bffc0368a41873cac86051bed9f0160c7ab6091c', '36314755364', 'Sopron'),
(29, 'Rácz', 'Balázs', 'balazs@proba.com', '91c9e39b6c8a622730044bfb8c6e4e8f633bb258', '36314755365', 'Eger'),
(30, 'Fekete', 'Gyula', 'gyula@proba.com', '9414aab2418f78cc98f24e81d4f5575cc4c3d82f', '36314755366', 'Nagykanizsa'),
(31, 'Szilágyi', 'Mihály', 'mihaly@proba.com', '2fd85210588c65d56b449efe869a9199d15a76f6', '36314755367', 'Dunaújváros'),
(32, 'Török', 'Károly', 'karoly@proba.com', '0e864258c77fbf6b21f73dbb45d8b1f837521cbc', '36314755368', 'Hódmezővásárhely'),
(33, 'Fehér', 'Róbert', 'robert@proba.com', '8bae5a9f7b06ac8101216d8aae488b3514113732', '36314755369', 'Dunakeszi'),
(34, 'Balázs', 'Béla', 'bela@proba.com', '70fe5a0c24a54368d8cf070c8660828e6cee9ec4', '36314755370', 'Cegléd'),
(35, 'Gál', 'Dávid', 'david@proba.com', '5ad7ac9412efd3cb9bc0fa558b7b880443ec30bd', '36314755371', 'Baja'),
(36, 'Kis', 'Dániel', 'daniel@proba.com', 'f700a6934e78cd908cb5665cd84f89318bfa2d43', '36314755372', 'Salgótarján'),
(37, 'Szűcs', 'Ádám', 'adam@proba.com', '92f6073b3fcbc0dd635c0494d4c0313e5544ecdd', '36314755373', 'Szigetszentmiklós'),
(38, 'Kocsis', 'Krisztián', 'krisztian@proba.com', '669fab6d6184ad3e5d7b1a16299ee7547fe67138', '36314755374', 'Vác'),
(39, 'Orsós', 'Miklós', 'miklos@proba.com', '6b6b36512fb7626778aa6860d0d9b85451fdb001', '36314755375', 'Gödöllő'),
(40, 'Pintér', 'Norbert', 'norbert@proba.com', '98c32dbf5d16f6cb256119ffe9300697453197e9', '36314755376', 'Ózd'),
(41, 'Fodor', 'Bence', 'bence@proba.com', 'ac22a02b67490c192fe5d8856b2484aa638bfdf7', '36314755377', 'Szekszárd'),
(42, 'Szalai', 'Máté', 'mate@proba.com', '7f4a4bd09a166846a5b4c10598add848c2642192', '36314755378', 'Mosonmagyaróvár'),
(43, 'Sipos', 'Pál', 'pal@proba.com', '479f2617d07e05d85ee86d7cc2348a293383a98e', '36314755379', 'Gyöngyös'),
(44, 'Magyar', 'Szabolcs', 'szabolcs@proba.com', 'ee8942f3240a0ad9239ece8ec6757b87a9c9f472', '36314755380', 'Pápa'),
(45, 'Lukács', 'Roland', 'roland@proba.com', 'fe4829e82dff1e2cf78f763a788e04c2b50258c5', '36314755381', 'Gyula'),
(46, 'Gulyás', 'Gergő', 'gergo@proba.com', 'dcafde96e4a1215779b9687dd0fb1850dd93ca86', '36314755382', 'Hajdúböszörmény'),
(47, 'Biró', 'Antal', 'antal@proba.com', '7838f3bc810fa9eba08a0ca639783793354b113e', '36314755383', 'Esztergom'),
(48, 'Király', 'Bálint', 'balint@proba.com', '0a1dc661d43ce2ea08fa118ab02815e41903778d', '36314755384', 'Kiskunfélegyháza'),
(49, 'László', 'Richárd', 'richard@proba.com', '5880194514ce16c17526bfcae48e784088997e32', '36314755385', 'Budaörs'),
(50, 'Balog', 'Márk', 'mark@proba.com', 'b70662cdd0f98f819bbea833303d163d6233d59b', '36314755386', 'Ajka'),
(51, 'Katona', 'Levente', 'levente@proba.com', '458f0e58d64f06ce85f28274d57c2ac8f344e132', '36314755387', 'Orosháza'),
(52, 'Bogdán', 'Gergely', 'gergely@proba.com', '0a93afb56a12caaab767c9980223f9ed49b496c8', '36314755388', 'Kazincbarcika'),
(53, 'Jakab', 'Ákos', 'akos@proba.com', '48748463d1fd1b8b1e98331f087187031fb6449c', '36314755389', 'Szentes'),
(54, 'Sándor', 'Viktor', 'viktor@proba.com', '96e66d14843d74171ef5621c0efafb984ec2cd23', '36314755390', 'Kiskunhalas'),
(55, 'Boros', 'Árpád', 'arpad@proba.com', 'b13cebdbaa49ccbeae197eb1d53b7434254e0dee', '36314755391', 'Jászberény'),
(56, 'Váradi', 'Géza', 'geza@proba.com', 'af700994eb99813b3a2fc1af5d01fe599e230fe8', '36314755392', 'Szentendre'),
(57, 'Fazekas', 'Márton', 'marton@proba.com', '50b38bc6ebae04d9fbdee687fb1c45719a2f847b', '36314755393', 'Komló'),
(58, 'Kelemen', 'Kristóf', 'kristof@proba.com', '05005fad8a8e514387eb67592580d496fde6ca90', '36314755394', 'Tata'),
(59, 'Antal', 'Jenő', 'jeno@proba.com', '46794246e89ea273e91aa2242ee5c0d8cb12ffe6', '36314755395', 'Nagykőrös'),
(60, 'Orosz', 'Kálmán', 'kalman@proba.com', '5ce4871d42f4e31072dfbc760c7d9fabb45b15c3', '36314755396', 'Siófok'),
(61, 'Somogyi', 'Patrik', 'patrik@proba.com', 'e4d2b8861e2323edea9e858191fa8f345c16c295', '36314755397', 'Makó'),
(62, 'Fülöp', 'Martin', 'martin@proba.com', '996c1e9dd29c03d62a4e39e1556b50af5403adf9', '36314755398', 'Gyál'),
(63, 'Veres', 'Milán', 'milan@proba.com', '4cca2ba3c4141fd64d34d98a1181e69294f933ac', '36314755399', 'Hajdúszoboszló'),
(64, 'Vincze', 'Barnabás', 'barnabas@proba.com', 'c42806b882d5df9b534615d6ff44042390f5e5bd', '36314755400', 'Törökszentmiklós'),
(65, 'Budai', 'Dominik', 'dominik@proba.com', '2f7edc12fabe35414589d2b0c0c5dfc2924f60dd', '36314755401', 'Vecsés'),
(66, 'Hegedűs', 'Marcell', 'marcell@proba.com', 'a8f01982108ce4f552e35f5ba344de0a4524897d', '36314755402', 'Keszthely'),
(67, 'Deák', 'Ernő', 'erno@proba.com', 'e2d9f18a1ed3d4dfc8abef54c86caedd37862a2c', '36314755403', 'Várpalota'),
(68, 'Pap', 'Mátyás', 'matyas@proba.com', 'fbaa71d06b3a751ec3b0d37b0cd138bd708d721f', '36314755404', 'Budapest'),
(69, 'Bálint', 'Endre', 'endre@proba.com', '5f03c7111ba9c13305b2332325ab467764c4f611', '36314755405', 'Debrecen'),
(70, 'Illés', 'Áron', 'aron@proba.com', '2e0ed9b563e289e98fd5361811e4263270bfa419', '36314755406', 'Szeged'),
(71, 'Pál', 'Dezső', 'dezso@proba.com', '226dd743294ceafa20ba67664b5cd101ac93400b', '36314755407', 'Miskolc'),
(72, 'Vass', 'Botond', 'botond@proba.com', '28d88209c53c8fc8ba3579e18dee552e808b7e96', '36314755408', 'Pécs'),
(73, 'Szőke', 'Nándor', 'nandor@proba.com', 'be966e93f0b7a84ab2db9da64330dc10ee6dc95c', '36314755409', 'Győr'),
(74, 'Fábián', 'Zsombor', 'zsombor@proba.com', 'b7a89cb41dae6c95cf1f471c301fc4a3f3119a42', '36314755410', 'Nyíregyháza'),
(75, 'Vörös', 'Szilárd', 'szilard@proba.com', 'ed0cbf3aa1932fc78e8bf4ca56ddcc3ff5f52add', '36314755411', 'Kecskemét'),
(76, 'Lengyel', 'Erik', 'erik@proba.com', 'bfd680e8863f6744bb69628a185b5cfe38b79aa1', '36314755412', 'Székesfehérvár'),
(77, 'Bognár', 'Olivér', 'oliver@proba.com', '1252626215e3fddd8c9a88659bbed7d25f770cd1', '36314755413', 'Szombathely'),
(78, 'Bodnár', 'Alex', 'alex@proba.com', '8c710486ceb03f08de3ebdae34ead9f249a2f699', '36314755414', 'Szolnok'),
(79, 'Jónás', 'Vilmos', 'vilmos@proba.com', 'a5810ca8707ef8ce2b409292812e0fd17836bc16', '36314755415', 'Tatabánya'),
(80, 'Szücs', 'Ottó', 'otto@proba.com', '6b22ced03103c25b845b2526cc976fe5276ead6e', '36314755416', 'Kaposvár'),
(81, 'Hajdu', 'Benedek', 'benedek@proba.com', '481b9ba48c2b8371ac71ee7c00cebeb8fda55347', '36314755417', 'Érd'),
(82, 'Halász', 'Dénes', 'denes@proba.com', 'e3d5081f6e50a5ee957c133182cdd29175cd3365', '36314755418', 'Veszprém'),
(83, 'Máté', 'Kornél', 'kornel@proba.com', '868aff73c43e8234c5f2c072b2959d5e7f744e2d', '36314755419', 'Békéscsaba'),
(84, 'Székely', 'Bertalan', 'bertalan@proba.com', '2fdc4ded96385e98898fff1fe1189e342db25ede', '36314755420', 'Zalaegerszeg'),
(85, 'Kozma', 'Benjámin', 'benjamin@proba.com', '1d8ac9414658884884be909c35af71f8349d6561', '36314755421', 'Sopron'),
(86, 'Gáspár', 'Zalán', 'zalan@proba.com', '8379fdcd17cc99353a8bc1ecbf3097b0ca622370', '36314755422', 'Eger'),
(87, 'Pásztor', 'Kevin', 'kevin@proba.com', '802d3be54d783f4bc3ebcfd38dc0a1b9ffa1ef3d', '36314755423', 'Nagykanizsa'),
(88, 'Bakos', 'Adrián', 'adrian@proba.com', '58e71360c3359fdde64fef0b005892eb85443391', '36314755424', 'Dunaújváros'),
(89, 'Dudás', 'Rudolf', 'rudolf@proba.com', '8f4ada8efa674163c6ffc02096fb3986d4ac366a', '36314755425', 'Hódmezővásárhely'),
(90, 'Major', 'Albert', 'albert@proba.com', '7679357857a838cae279d6123d61d629d38f32b7', '36314755426', 'Dunakeszi'),
(91, 'Orbán', 'Vince', 'vince@proba.com', 'd593bcc52c304849532d6627aeea2c255ca59365', '36314755427', 'Cegléd'),
(92, 'Virág', 'Ervin', 'ervin@proba.com', '7f217cf5f9deec3cc4ab7707d737e43f62886729', '36314755428', 'Baja'),
(93, 'Hegedüs', 'Győző', 'gyozo@proba.com', '7498c6bfb05e8666aaefe2b4e071f2256447a03d', '36314755429', 'Salgótarján'),
(94, 'Barna', 'Zsigmond', 'zsigmond@proba.com', '64cc43accc40831c35605e51ddd19cdc807bdbea', '36314755430', 'Szigetszentmiklós'),
(95, 'Novák', 'Andor', 'andor@proba.com', '862dab9cb913d6992a598428f21e44efd82f9961', '36314755431', 'Vác'),
(96, 'Soós', 'Gusztáv', 'gusztav@proba.com', 'c4176f81cc2066788ac669e781c1b5ca3b9ef7aa', '36314755432', 'Gödöllő'),
(97, 'Tamás', 'Szilveszter', 'szilveszter@proba.com', '76295bb405136c30d603d649c947ce35632b048b', '36314755433', 'Ózd'),
(98, 'Nemes', 'Iván', 'ivan@proba.com', '09531e9b778e3f9dc87a04cf231b61953101a967', '36314755434', 'Szekszárd'),
(99, 'Pataki', 'Noel', 'noel@proba.com', '4d602195a67c5761e0948e284b337dadabe3ca58', '36314755435', 'Mosonmagyaróvár'),
(100, 'Balla', 'Barna', 'barna@proba.com', 'e6efba5225fa3171cb51ea3bd7622e578b5ac133', '36314755436', 'Gyöngyös'),
(101, 'Faragó', 'Elemér', 'elemer@proba.com', '1d5811940cdf285f1c573c10980603dd330eb625', '36314755437', 'Pápa'),
(102, 'Kerekes', 'Arnold', 'arnold@proba.com', '023a2d92541ebf75b7a6437379e501a2a28f820d', '36314755438', 'Gyula'),
(103, 'Borbély', 'Csongor', 'csongor@proba.com', '84c678a0f91f72adce8ee2d97ac0bd653938a4f3', '36314755439', 'Hajdúböszörmény'),
(104, 'Barta', 'Ábel', 'abel@proba.com', 'f3db6955a6c03b83bf4add5382b473992eab2af7', '36314755440', 'Esztergom'),
(105, 'Péter', 'Krisztofer', 'krisztofer@proba.com', '0f21409c7d0f6833e6911465c9375f0d91a5131e', '36314755441', 'Kiskunfélegyháza'),
(106, 'Csonka', 'Emil', 'emil@proba.com', 'bbe193eba49253efb1a7cdde98269f1f87037d48', '36314755442', 'Budaörs'),
(107, 'Mezei', 'Tivadar', 'tivadar@proba.com', '8ab4d71b67d182fcf8156ff233b49b3a331ec124', '36314755443', 'Ajka'),
(108, 'Sárközi', 'Hunor', 'hunor@proba.com', 'd1a84b93ced03eac64386836b2a78abef2ac92c5', '36314755444', 'Orosháza'),
(109, 'Szekeres', 'Bendegúz', 'bendeguz@proba.com', '5abe63fdb116fdd02a045064682f6b8b1ac7c7e5', '36314755445', 'Kazincbarcika'),
(110, 'Márton', 'Henrik', 'henrik@proba.com', 'ddb70d63efd37b070ca00410d013124e97c186d5', '36314755446', 'Szentes');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `fokategoria`
--

CREATE TABLE `fokategoria` (
  `fokategoria_ID` int(11) NOT NULL,
  `fokategoria_megnevezes` varchar(1000) COLLATE utf8mb4_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `fokategoria`
--

INSERT INTO `fokategoria` (`fokategoria_ID`, `fokategoria_megnevezes`) VALUES
(1, 'Műszaki cikk'),
(2, 'Számítógép'),
(3, 'Háztartási gép'),
(4, 'Divat és ruházat'),
(5, 'Sport és fitness'),
(6, 'Otthon és kert'),
(7, 'Autó, motor'),
(8, 'Játék'),
(9, 'Irodatechnika');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoria_kivansaglistaban`
--

CREATE TABLE `kategoria_kivansaglistaban` (
  `kategoria_ID` int(11) NOT NULL,
  `kivansaglista_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kivansaglista`
--

CREATE TABLE `kivansaglista` (
  `kivansaglista_ID` int(11) NOT NULL,
  `felhasznalo_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `licit`
--

CREATE TABLE `licit` (
  `licit_ID` int(11) NOT NULL,
  `licitalo_ID` int(11) NOT NULL,
  `licitalas_ID` int(11) NOT NULL,
  `ertek` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `licitalas`
--

CREATE TABLE `licitalas` (
  `licitalas_ID` int(11) NOT NULL,
  `termek_ID` int(11) NOT NULL,
  `kezdo_idopont` datetime NOT NULL,
  `veg_idopont` datetime NOT NULL,
  `vegso_ar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek`
--

CREATE TABLE `termek` (
  `termek_id` int(11) NOT NULL,
  `felhasznalo_id` int(11) NOT NULL,
  `alkategoria_id` int(11) NOT NULL,
  `meghirdetes_ideje` datetime NOT NULL,
  `termek_megnevezes` varchar(1000) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `termek_leiras` text COLLATE utf8mb4_hungarian_ci NOT NULL,
  `termek_ar` int(11) NOT NULL,
  `termek_allapot` varchar(1000) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `kep` varchar(1000) COLLATE utf8mb4_hungarian_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `termek`
--

INSERT INTO `termek` (`termek_id`, `felhasznalo_id`, `alkategoria_id`, `meghirdetes_ideje`, `termek_megnevezes`, `termek_leiras`, `termek_ar`, `termek_allapot`) VALUES
(5, 11, 1, '2020-04-09 23:11:27', 'Huawei P40 Lite mobitelefon', 'Mobiltelefon - 6,4\"-es kijelző, 2310 × 1080, IPS kijelző, 8 magos processzor, 6 GB RAM, 128 GB belső tárhely', 90490, 'Bontatlan'),
(6, 11, 2, '2020-04-09 23:11:27', 'Sony Bravia KD-85ZG9B TV', '8K HDR ragyogó képének és a MASTER Series egyedi filozófiájának ötvözete.', 4674900, 'Új'),
(7, 56, 24, '2020-04-09 23:11:27', 'Aldo Granada kerekes bevásárlótáska (banyatank)', 'A kényelmes fogantyút kellemes a kézben tartani. A kerekei kellően tartósak és szilárdak. A textil része levehető és mosható.', 10395, 'Megviselt'),
(8, 45, 13, '2020-04-09 23:11:27', 'WETHEPEOPLE Nova 20 BMX Kerékpár', 'Ideaális annak, aki most szeretne belekóstolni a BMX világába', 85999, 'Bontatlan'),
(9, 65, 17, '2020-04-09 23:11:27', 'Educa Élet: A nagy kihívás 24000 db-os', 'Az óriás panoráma  képkirakó az Élet címet viseli. Avíz felett vadállatok futnak, a víz alatt pedig Atlantisz tárul fel. A kép nagysága összeillesztés után  428*157 cm.', 51710, 'Kirakatlan'),
(10, 64, 27, '2020-04-09 23:11:27', 'Cassida Integral 3.0 MF', 'Gyárilag jár hozzá; sötétített és átlátszó plexi, Beépített napszemüveg. Mosható anyagú, lélegző bélés. ', 25990, 'Használt'),
(11, 12, 21, '2020-04-09 23:11:27', 'Lexmark CX725de (40C9554)', ' A kapacitás 650 oldal (2300-ig bővíthető), a készülék támogatja a kisméretű hordozókat és a szalagpapírhosszt is', 680080, 'Új'),
(12, 56, 4, '2020-04-09 23:11:27', 'Apple MacBook Air 13 Mid 2017 MQD32 Notebook', 'Akár 12 órás üzemidő. Gyors, könnyű, erős. Az e‑mailezéshez, az internetezéshez, a szöveges üzenetek kezeléséhez és a FaceTime-hívásokhoz is nagyszerű alkalmazások biztosítják a hátteret', 279980, 'Használt'),
(13, 54, 8, '2020-04-09 23:13:06', 'Bosch KGV58VL31S Hűtőszekrény, hűtőgép', 'A Bosch KGV58VL31S hűtőszekrény bőséges helyet kínáló CrisperBox rekesze szabályozott páratartalommal gondoskodik arról, hogy a zöldségek és gyümölcsök hosszú ideig maradjanak frissek és ropogósak. ', 197900, 'Bontatlan'),
(14, 24, 22, '2020-04-09 23:13:06', 'Noblechairs ICON (NBL-ICN-PU)', 'Teljes magasság (alap): kb. 1280-1380 cm, - Maximális súly (felhasználó): 180 kg', 131000, 'Használt'),
(15, 15, 15, '2020-04-09 23:13:06', 'LEGO Architecture - Buckingham Palota (21029)', 'Ez a részletes kidolgozású LEGO modell a palota monumentális, neoklasszikus előrészére összpontosít, amelyet Sir Aston Webb építész tervezett 1913-ban és amely a híres erkélynek, előkertnek és palotakapuknak ad helyet.', 28900, 'Új'),
(16, 110, 5, '2020-04-09 23:13:06', 'Dell U2718Q Monitor', 'Magával ragadó látvány: A Dell HDR technológiája a nagyobb színmélységet, a lenyűgöző élességet és az árnyalatok magas kontrasztarányát kombinálva lenyűgözően valósághű képeket alkot. ', 189900, 'Tesztelt'),
(17, 82, 11, '2020-04-09 23:13:06', 'Rövid ballonkabát bonprix 971246', 'Teljes hossza a 42-es méretben kb. 74 cm, géppel mosható.', 8999, 'Használt'),
(18, 76, 6, '2020-04-09 23:13:06', 'Canon PIXMA MG3650S (0515C106) Nyomtató', 'A kiváló minőségű, szegély nélküli nyomatokat gyorsan biztosító wifis multifunkciós készülékkel úgy csatlakozhat, nyomtathat, másolhat és szkennelhet, ahogyan csak szeretne ', 23990, 'Új'),
(19, 88, 25, '2020-04-09 23:13:06', 'Corby 5721 gördeszka', 'Cápamintás gyerekgördeszka kalandoroknak', 3790, 'Használt'),
(20, 33, 3, '2020-04-09 23:13:06', 'BlitzWolf BW-FYE7 fülhallgató', 'A fülhallgatót mély, határozott basszus és tiszta énekhangok jellemzik. Akár rockot, akár hip-hopot, akár komolyzenét vagy más műfajt kedvelsz, egyaránt minőségi és élvezetes zenehallgatásban lehet részed.', 15790, 'Új'),
(21, 86, 9, '2020-04-09 23:13:06', 'Whirlpool AWE 50510', 'A felültöltős Whirlpool AWE 50510 mosógép kisebb fürdőszobákban, mosókonyhákban is elfér, így nagyszerű választás, ha hely szűkében vagy.', 79880, 'Használt'),
(22, 55, 16, '2020-04-09 23:13:06', 'Hasbro Rizikó', 'A Rizikó egy több mint ötven éves stratégiai hódító játék, ami mind a mai napig nagy népszerűségnek örvend köszönhetően az egyszerű játékszabályainak, ami könnyű tanulhatóságot, mégis roppant izgalmas játékmenetet eredményez.', 12390, 'Bontatlan'),
(23, 56, 13, '2020-04-09 23:13:06', 'Koliken Gommer Kerékpár', 'Próbáld ki milyen három keréken az élet!', 99990, 'Új'),
(24, 64, 3, '2020-04-09 23:13:06', ' JBL C45 BT', 'Akár 16 órás üzemidő, A vezetékes módban a legtöbb okostelefonnal kompatibilis, mellékelt kábel lehetővé teszi a könnyű vezérlést. ', 24190, 'Bontatlan'),
(25, 27, 7, '2020-04-09 23:13:06', 'Philips FC9552/09 PowerPro Active', 'A Clean-air szűrőrendszer tökéletes szigetelést biztosít, és felfogja a finom por legalább 99,9%-át – ideértve a polleneket, szőr és poratkákat – az allergiásoknak, és azoknak, akik magasabb szintre emelnék a higiéniát. ', 46890, 'Használt'),
(26, 84, 23, '2020-04-09 23:13:07', 'Casio fx-570ES PLUS', 'Természetes algebrai bevitelt biztosító kijelzővel és felsőfokú tanulmányokhoz szükséges funkciókkal rendelkezik. A nehéz feladatok megoldásához egyszerűen kezelhető számológép dukál. ', 8490, 'Vadonatúj'),
(29, 15, 20, '2020-04-09 23:16:39', 'Bosch S3 12V 45Ah 400A Jobb+', 'A következő autókba ajánljuk: ALFA ROMEO, AUDI, BMW, CITROEN, CHRYSLER, DACIA, FIAT, LANCIA, MERCEDES, PEUGEOT, PORSCHE, ROVER, SAAB, SEAT, SKODA, SMART, VOLVO', 16300, 'Működőképes'),
(30, 12, 1, '2020-04-09 23:16:39', 'Bea-fon SL250', 'Típus: Bartype Phone, Méretek: 112,5 x 51,5 x 12,8 mm, Kijelző típusa: TFT Display ', 14990, 'Új'),
(31, 58, 16, '2020-04-09 23:16:39', 'Lucky Duck Games Bűnügyi krónikák', 'A Bűnügyi krónikák egy valóság közeli élményt nyújtó játék, ahol együtt, közösen kell kinyomoznotok a bűnügyeket. A játék különlegessége, hogy szükséges hozzá egy alkalmazás a telefonodra. ', 7850, 'Bontatlan'),
(32, 53, 2, '2020-04-09 23:16:39', 'Maxxo 3981 Tv', 'Táskában is hordozható készülék, mely alkalmas buszokra való felszerelésre is', 31400, 'Használt'),
(33, 14, 10, '2020-04-09 23:16:39', 'Dunlop Pricemastor sav- és lúgálló PVC csizma', 'Ellenáll mindennek; savnak, lúgnak, fertőtlenítőszereknek. A vastag, fröcsöntött talp csúszásbiztossá teszi a terméket', 2911, 'Vadonatúj'),
(34, 22, 26, '2020-04-09 23:16:39', 'Riwall RPM 5135 önjáró fűnyíró', '4v1 funkciója lehetővé teszi a gyűjtést, talajtakarást, hátulkidobást és az oldalkidobást. ', 83990, 'Új'),
(35, 14, 1, '2020-04-09 23:16:39', 'Apple iPhone 11 64GB', 'Nagyon el van találva. Művészet vele rossz fotót lőni. Kétszeres mélységig vízálló. A legbiztonságosabb arcazonosítás okos­telefonon. ', 245830, 'Bontatlan'),
(36, 33, 6, '2020-04-09 23:16:39', 'HP DeskJet 2630 nyomtató', 'Dinamikus biztonságot nyújtó nyomtató. Csak eredeti HP áramkört használó tintapatronokkal használható. ', 16701, 'Használt'),
(37, 96, 23, '2020-04-09 23:16:39', ' Canon P1-DTSC II számológép', 'Hordozható 12 számjegyes számológép nyomtatóval, dupla ellenőrzéssel, valamint adózási, üzleti, és időkiszámítási funkciókkal. ', 6090, 'Bontatlan'),
(38, 22, 24, '2020-04-09 23:16:39', 'Eberlestock Skycrane II hátizsák', 'Ő itt Hát Izsák a 137 literig bővíthető hátizsák', 198800, 'Vadonatúj'),
(39, 78, 22, '2020-04-09 23:16:39', 'Q229 - gamer szék', 'Sportos, ergonomikus kialakítású fotel multiblock mechanikával. Kényelmi funkciói lévén több órás munkavégzést vagy játékot tesz lehetővé.', 68400, 'Használt'),
(40, 87, 15, '2020-04-09 23:16:39', 'LEGO Hobbit - Egy váratlan összejövetel ', 'Szürke Gandalf meghívta a törpöket Zsáklakba - Zsákos Bilbó hobbit-üregébe. Emeld le a hobbit-üreg tetejét, és szórakoztasd a törpöket étel- és italkészletekkel! ', 99900, 'Bontatlan'),
(41, 93, 7, '2020-04-09 23:16:40', 'Electrolux ZB 5003 Rapido, a morzsaporszívó', 'A Rapido - az egyetlen kerekekkel rendelkező akkumulátoros morzsaporszívó. A kerekek szerepe nagyon egyszerű: tehermentesítik a kezet és a kart.', 12980, 'Bontatlan'),
(42, 48, 5, '2020-04-09 23:16:40', 'ASUS VG248QE monitor', 'A készülék kizárólag külön digitális vevődekóder csatlakoztatásával válik alkalmassá a magyarországi földfelszíni, szabad hozzáférésű digitális televíziós műsorszórás vételére. ', 65999, 'Használt'),
(43, 107, 10, '2020-04-09 23:16:40', 'Leon Comfortstep PU 101 klumpa', ' Felsőrész anyaga: valódi bőr felső, Belsőrész anyaga: bőr belső, Talprész anyaga: szintetikus talp, Extra tulajdonság: csúszásmentes talp', 7400, 'Új'),
(44, 79, 27, '2020-04-09 23:16:40', 'Hecht 52631', 'A vonzó és modern design minden robogó és motorkerékpár tulajdonos tetszését elnyeri. ', 8500, 'Használt');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_kivansaglistaban`
--

CREATE TABLE `termek_kivansaglistaban` (
  `termek_ID` int(11) NOT NULL,
  `kivansaglista_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `vasarlas`
--

CREATE TABLE `vasarlas` (
  `vasarlas_ID` int(11) NOT NULL,
  `felhasznalo_ID` int(11) NOT NULL,
  `termek_ID` int(11) NOT NULL,
  `vasarlas_idopont` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `alkategoria`
--
ALTER TABLE `alkategoria`
  ADD PRIMARY KEY (`alkategoria_ID`),
  ADD KEY `fokategoria` (`fokategoria_ID`);

--
-- A tábla indexei `ertekeles`
--
ALTER TABLE `ertekeles`
  ADD PRIMARY KEY (`ertekeles_ID`),
  ADD KEY `vasarlas` (`vasarlas_ID`);

--
-- A tábla indexei `felhasznalo`
--
ALTER TABLE `felhasznalo`
  ADD PRIMARY KEY (`felhasznalo_ID`),
  ADD UNIQUE KEY `email_cim` (`email_cim`);

--
-- A tábla indexei `fokategoria`
--
ALTER TABLE `fokategoria`
  ADD PRIMARY KEY (`fokategoria_ID`);

--
-- A tábla indexei `kategoria_kivansaglistaban`
--
ALTER TABLE `kategoria_kivansaglistaban`
  ADD PRIMARY KEY (`kategoria_ID`,`kivansaglista_ID`),
  ADD KEY `kivansaglista_FK1` (`kivansaglista_ID`);

--
-- A tábla indexei `kivansaglista`
--
ALTER TABLE `kivansaglista`
  ADD PRIMARY KEY (`kivansaglista_ID`),
  ADD KEY `felhasznalo_FK` (`felhasznalo_ID`);

--
-- A tábla indexei `licit`
--
ALTER TABLE `licit`
  ADD PRIMARY KEY (`licit_ID`),
  ADD KEY `licitalo` (`licitalo_ID`),
  ADD KEY `licitalas` (`licitalas_ID`);

--
-- A tábla indexei `licitalas`
--
ALTER TABLE `licitalas`
  ADD PRIMARY KEY (`licitalas_ID`),
  ADD KEY `termek_FK` (`termek_ID`);

--
-- A tábla indexei `termek`
--
ALTER TABLE `termek`
  ADD PRIMARY KEY (`termek_id`),
  ADD KEY `felh` (`felhasznalo_id`),
  ADD KEY `alkategoria` (`alkategoria_id`);

--
-- A tábla indexei `termek_kivansaglistaban`
--
ALTER TABLE `termek_kivansaglistaban`
  ADD PRIMARY KEY (`termek_ID`,`kivansaglista_ID`),
  ADD KEY `kivansaglista_FK` (`kivansaglista_ID`);

--
-- A tábla indexei `vasarlas`
--
ALTER TABLE `vasarlas`
  ADD PRIMARY KEY (`vasarlas_ID`),
  ADD KEY `felhasznalo` (`felhasznalo_ID`),
  ADD KEY `termek` (`termek_ID`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `alkategoria`
--
ALTER TABLE `alkategoria`
  MODIFY `alkategoria_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT a táblához `ertekeles`
--
ALTER TABLE `ertekeles`
  MODIFY `ertekeles_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `felhasznalo`
--
ALTER TABLE `felhasznalo`
  MODIFY `felhasznalo_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT a táblához `fokategoria`
--
ALTER TABLE `fokategoria`
  MODIFY `fokategoria_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT a táblához `kivansaglista`
--
ALTER TABLE `kivansaglista`
  MODIFY `kivansaglista_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `licit`
--
ALTER TABLE `licit`
  MODIFY `licit_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `licitalas`
--
ALTER TABLE `licitalas`
  MODIFY `licitalas_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `termek`
--
ALTER TABLE `termek`
  MODIFY `termek_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT a táblához `vasarlas`
--
ALTER TABLE `vasarlas`
  MODIFY `vasarlas_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `alkategoria`
--
ALTER TABLE `alkategoria`
  ADD CONSTRAINT `fokategoria` FOREIGN KEY (`fokategoria_ID`) REFERENCES `fokategoria` (`fokategoria_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `ertekeles`
--
ALTER TABLE `ertekeles`
  ADD CONSTRAINT `vasarlas` FOREIGN KEY (`vasarlas_ID`) REFERENCES `vasarlas` (`vasarlas_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `kategoria_kivansaglistaban`
--
ALTER TABLE `kategoria_kivansaglistaban`
  ADD CONSTRAINT `kategoria` FOREIGN KEY (`kategoria_ID`) REFERENCES `alkategoria` (`alkategoria_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kivansaglista_FK1` FOREIGN KEY (`kivansaglista_ID`) REFERENCES `kivansaglista` (`kivansaglista_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `kivansaglista`
--
ALTER TABLE `kivansaglista`
  ADD CONSTRAINT `felhasznalo_FK` FOREIGN KEY (`felhasznalo_ID`) REFERENCES `felhasznalo` (`felhasznalo_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `licit`
--
ALTER TABLE `licit`
  ADD CONSTRAINT `licitalas` FOREIGN KEY (`licitalas_ID`) REFERENCES `licitalas` (`licitalas_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `licitalo` FOREIGN KEY (`licitalo_ID`) REFERENCES `felhasznalo` (`felhasznalo_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `licitalas`
--
ALTER TABLE `licitalas`
  ADD CONSTRAINT `termek_FK` FOREIGN KEY (`termek_ID`) REFERENCES `termek` (`termek_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `termek`
--
ALTER TABLE `termek`
  ADD CONSTRAINT `alkategoria` FOREIGN KEY (`alkategoria_id`) REFERENCES `alkategoria` (`alkategoria_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `felh` FOREIGN KEY (`felhasznalo_id`) REFERENCES `felhasznalo` (`felhasznalo_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `termek_kivansaglistaban`
--
ALTER TABLE `termek_kivansaglistaban`
  ADD CONSTRAINT `kivansaglista_FK` FOREIGN KEY (`kivansaglista_ID`) REFERENCES `kivansaglista` (`kivansaglista_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `termek_FK1` FOREIGN KEY (`termek_ID`) REFERENCES `termek` (`termek_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `vasarlas`
--
ALTER TABLE `vasarlas`
  ADD CONSTRAINT `felhasznalo` FOREIGN KEY (`felhasznalo_ID`) REFERENCES `felhasznalo` (`felhasznalo_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `termek` FOREIGN KEY (`termek_ID`) REFERENCES `termek` (`termek_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
